# splunk - Deploy the Splunk Universal Forwarder within the Stanford ecosphere

This is a puppet module that deploys the Splunk Universal Forwarder for IEDO clients.  It can be modified to support other groups, but it inherently is configured to support the IEDO Splunk Client Certificate.

# Prerequisites/Dependencies

* This module assumes that the Splunk Universal Forwarder package is available via external repo (http://debian.stanford.edu/debian-stanford)
* wallet module:  This module will need to be included in your Puppet manifest:
```bash
mod 'wallet',
  :git => 'https://code.stanford.edu/pe-public/wallet.git',
  :tag => 'v1.2.0'
```
* Using the wallet module, this Splunk Module will request the Splunk SSL client key from wallet under the following location: 
```bash
ssl-key/${certificate_name}
```
Where ${certificate_name} is a Puppet Module Parameter.

# Puppet Module Usage
Simply include this module in the puppet code
```bash
include splunk
```

Or Puppet Module Parameters can be specified in this way:
```bash
class { 'splunk':
  ensure           => 'present',
  certificate_name => 'splunk-iedo.stanford.edu'
}
```

And Puppet Module Parameters can also be specified via Hiera in the following format:
```bash
splunk::certificate_name:      'splunk-iedo.stanford.edu'
splunk::splunk_forwarder_host: 'susplunkdmgr.stanford.edu'
```

## Puppet Module Parameters

### ensure (Enum: 'absent', 'present') - detault: 'present'
Determines if splunk is installed or uninstalled from the system.
```bash
splunk::ensure:     ['absent', 'present']
```

### certificate_name (String) - detault: 'splunk-iedo.stanford.edu'
The name of the Splunk Client Certificate.  The Client Certificate is expected to be stored in files/etc/splunkforwarder/auth/su-certs and this certificate name is also expected to be used to store the Splunk Client Key in Wallet: ssl-key/splunk-iedo.stanford.edu.

### splunk_forwarder_host (String) - detault: 'susplunkdmgr.stanford.edu'
The hostname of the Stanford Splunk Deployment Server.  Maps to targetUri in templates/etc/splunkforwarder/system/local/deploymentclient.conf.erb

### splunk_forwarder_port (String) - detault: '8089'
The port of the Stanford Splunk Deployment Server.  Maps to targetUri in templates/etc/splunkforwarder/system/local/deploymentclient.conf.erb

### deployment_client_name (String) - detault: 'uit-coreinfra-unix-forwarder'
A name that the deployment server can filter on.  Please contact the Splunk Administrator for the proper value.  Maps to clientName in templates/etc/splunkforwarder/system/local/deploymentclient.conf.erb
