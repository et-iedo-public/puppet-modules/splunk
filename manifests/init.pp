# Install the splunk universal forwarder on a machine and configure it..

class splunk (
  Enum['absent', 'present'] $ensure = 'present',
  # This will be kept to complete the migration
  String $certificate_name          = 'iedo.susplunkforwarder.pem',

  String $splunk_forwarder_host     = 'susplunkdmgr.stanford.edu',
  String $splunk_forwarder_port     = '8089',
  String $deployment_client_name    = 'uit-coreinfra-unix-forwarder',
) {

  $splunk_home = '/opt/splunkforwarder'
  $splunk_etc  = '/opt/splunkforwarder/etc'

  # Resources that do not have to be inside the ensure conditional.

  # Create the splunk user and group
  accounts::user { 'splunk':
    ensure     => $ensure,
    uid        => '515',
    comment    => 'Splunk Server',
    home       => '/opt/splunk',
    system     => true,
    group      => 'splunk',
    shell      => '/bin/bash',
    managehome => false,
    groups     => [ 'ssl-cert', 'splunk' ],     
  }

  group { 'ssl-cert':      
    ensure => $ensure,
    name   => 'ssl-cert', 
    gid    => 525,
  }

  ### MAIN PORTION OF CODE ###
  # The Splunk Universal Forwarder installs into /opt/splunkforwarder.
  if ($ensure == 'present') {
    #
    # PRESENT
    #

    # Install the package
    package { 'splunkforwarder':
      ensure  => present,
      require => User['splunk'],
    }

    # We make some links to be more Debian-friendly.
    file {
      '/etc/splunkforwarder':
        ensure  => link,
        target  => $splunk_etc;
      '/var/log/splunkforwarder':
        ensure  => link,
        target  => "$splunk_home/var/log/splunk",
        require => Package['splunkforwarder'];
    }

    # This is where we put the certificate and key files.
    file {
      "$splunk_etc/auth":
        ensure  => directory,
        require => Package['splunkforwarder'];
      "$splunk_etc/auth/su-certs":
        ensure  => directory,
        require => Package['splunkforwarder'];
    }

    ##
    ## Client certificate
    ##

    $cacert_path     = "$splunk_etc/auth/su-certs/susplunkrootca.pem"
    $splunkcert_path = "$splunk_etc/auth/su-certs/susplunkforwarder.pem"

    file { '/opt/splunkforwarder/etc/splunk-launch.conf':
      ensure => 'present',
      source => 'puppet:///modules/splunk/opt/splunkforwarder/etc/splunk-launch.conf',
      notify => Service[splunkd],
    }

    # The susplunk CA root certificate
    file { $cacert_path:
      ensure  => present,
      source  => 'puppet:///modules/splunk/etc/splunkforwarder/auth/su-certs/susplunkrootca.pem',
      require => File["$splunk_etc/auth/su-certs"],
    }

    # The Splunk certificate
    wallet { "ssl-key/${certificate_name}":
      ensure  => 'present',
      type    => 'file',
      path    => $splunkcert_path,
      owner   => 'root',
      group   => 'root',
      mode    => '0600',
      verify  => true,
      require => [
        Package['splunkforwarder'],
        File["$splunk_etc/auth/su-certs"],
      ],
    }


    ##
    ## "Local" configuration files
    ##

    $splunk_local = "$splunk_etc/system/local"
    $deploy_conf  = "$splunk_local/deploymentclient.conf"
    $server_conf  = "$splunk_local/server.conf"

    file { $deploy_conf:
      ensure  => present,
      content => template('splunk/etc/splunkforwarder/system/local/deploymentclient.conf.erb'),
      notify  => Service[splunkd],
      require => Package['splunkforwarder'],
    }

    # Don't replace if exists (verify => false).
    wallet { 'config/iedo/splunk/server.conf':
      ensure  => 'present',
      type    => 'file',
      path    => $server_conf,
      owner   => 'root',
      group   => 'root',
      mode    => '0600',
      verify  => false,
      require => Package['splunkforwarder'],
      notify  => Service['splunkd'],
    }

    service { 'splunkd':
      ensure   => running,
      enable   => true,
      require  => [
        Package['splunkforwarder'],
        File['/lib/systemd/system/splunkd.service'],
        File[$deploy_conf],
        Wallet['config/iedo/splunk/server.conf'],
      ]
    }

    # Taken from https://answers.splunk.com/answers/59662/is-there-a-systemd-unit-file-for-splunk.html
    file { '/lib/systemd/system/splunkd.service':
      ensure  => present,
      content => template('splunk/lib/systemd/system/splunkd.service.erb'),
    }

    file { '/lib/systemd/system/SplunkForwarder.service':
      ensure => absent,
      notify => Exec['systemd_reload_daemon_splunk'],
    }

    exec { 'systemd_reload_splunk':
      path        => '/bin:/usr/bin',
      command     => 'systemctl restart splunkd',
      refreshonly => true,
      subscribe   => File['/opt/splunkforwarder/etc/splunk-launch.conf'],
    }

    ##### Keep these items around until Splunk Module migration is complete #####

    $outputs_conf  = "$splunk_local/outputs.conf"
    $key_path      = "$splunk_etc/auth/su-certs/splunk-iedo.stanford.edu.key"
    $cert_path     = "$splunk_etc/auth/su-certs/splunk-iedo.stanford.edu.pem"

    # 1. Install the private key. We use the same key-pair for most of
    # servers, only using service-specific key-pairs for the more
    # sensitive services such as kerberos.
    wallet { "ssl-key/splunk-iedo.stanford.edu":
      ensure  => 'absent',
      type    => 'file',
      path    => $key_path,
      owner   => 'root',
      group   => 'root',
      mode    => '0600',
      require => [
        Package['splunkforwarder'],
        File["$splunk_etc/auth/su-certs"],
      ],
    }

    # 2. Install the certificate from the usual cert-files location.
    file { $cert_path:
      ensure  => absent,
      require => File["$splunk_etc/auth/su-certs"],
    }

    file { $outputs_conf:
      ensure  => absent,
      notify  => Service[splunkd],
      require => Package['splunkforwarder'],
    }

    ##### Splunk Module Migration clean up end #####

  } else {
    #
    # ABSENT
    #

    # Uninstall the package
    package { 'splunkforwarder':
      ensure  => absent,
    }

    group { 'splunk':      
      ensure => 'absent',
    }

    file {
      '/etc/splunkforwarder':
        ensure => absent;
      '/var/log/splunkforwarder':
        ensure => absent;
      $splunk_home:
        ensure => absent,
        force  => true;
    }

    # (1) Stop service and then (2) uninstall systemd unit file.
    service { 'splunkd':
      ensure   => stopped,
      enable   => false,
    }
    file { '/lib/systemd/system/splunkd.service':
      ensure  => absent,
    }
  }

  # If the systemd service unit file changes be sure to reload the
  # systemd daemon.
  exec { 'systemd_reload_daemon_splunk':
    path        => '/bin:/usr/bin',
    command     => 'systemctl daemon-reload',
    refreshonly => true,
    subscribe   => File['/lib/systemd/system/splunkd.service'],
  }
}
